/* SPDX-FileCopyrightText: 2023 John Scott <jscott@posteo.net>
 * SPDX-License-Identifier: GPL-3.0-or-later */
#include <gpgme.h>
#include <locale.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void) {
	const char *const localestr = setlocale(LC_ALL, "");
	if(!localestr) {
		fputs("Failed to enable default locale\n", stderr);
		exit(EXIT_FAILURE);
	}

	if(!gpgme_check_version(NULL)) {
		fputs("Failed to initialize GPGME\n", stderr);
		exit(EXIT_FAILURE);
	}

	gpgme_error_t w = gpgme_set_locale(NULL, LC_ALL, localestr);
	if(w) {
		fprintf(stderr, "Failed to set default locale in GPGME: %s\n", gpgme_strerror(w));
		exit(EXIT_FAILURE);
	}

	gpgme_ctx_t ctx;
	w = gpgme_new(&ctx);
	if(w) {
		fprintf(stderr, "Failed to get GPGME context object: %s\n", gpgme_strerror(w));
		exit(EXIT_FAILURE);
	}
	w = gpgme_set_protocol(ctx, GPGME_PROTOCOL_OPENPGP);
	if(w) {
		fprintf(stderr, "Failed to set protocol to OpenPGP: %s\n", gpgme_strerror(w));
		gpgme_release(ctx);
		exit(EXIT_FAILURE);
	}

	gpgme_keylist_mode_t mode = gpgme_get_keylist_mode(ctx);
	mode |= GPGME_KEYLIST_MODE_SIGS;
	w = gpgme_set_keylist_mode(ctx, mode);
	if(w) {
		fprintf(stderr, "Failed to enable the listing of keys: %s\n", gpgme_strerror(w));
		gpgme_release(ctx);
		exit(EXIT_FAILURE);
	}

	gpgme_data_t data;
	w = gpgme_data_new_from_file(&data, "/usr/share/keyrings/debian-archive-bullseye-automatic.gpg", true);
	if(w) {
		fprintf(stderr, "Failed to read Debian Bullseye archive keyring: %s\n", gpgme_strerror(w));
		gpgme_release(ctx);
		exit(EXIT_FAILURE);
	}

	w = gpgme_op_keylist_from_data_start(ctx, data, false);
	if(w) {
		fprintf(stderr, "Failed to start reading keys: %s\n", gpgme_strerror(w));
		gpgme_data_release(data);
		gpgme_release(ctx);
		exit(EXIT_FAILURE);
	}

	gpgme_key_t key;
	w = gpgme_op_keylist_next(ctx, &key);
	if(w) {
		fprintf(stderr, "Failed to get first key: %s\n", gpgme_strerror(w));
		gpgme_data_release(data);
		gpgme_release(ctx);
		exit(EXIT_FAILURE);
	}

	if(key->uids->signatures) {
		puts("We got signatures!");
		gpgme_key_unref(key);
		w = gpgme_op_keylist_end(ctx);
		if(w) {
			fprintf(stderr, "Failed to list keys: %s\n", strerror(w));
			gpgme_data_release(data);
			gpgme_release(ctx);
			exit(EXIT_FAILURE);
		}
		gpgme_data_release(data);
		gpgme_release(ctx);
		exit(EXIT_SUCCESS);
	}

	fputs("We found no signatures.\n", stderr);
	gpgme_key_unref(key);
	w = gpgme_op_keylist_end(ctx);
	if(w) {
		fprintf(stderr, "Failed to list keys: %s\n", strerror(w));
	}
	gpgme_data_release(data);
	gpgme_release(ctx);
	exit(EXIT_FAILURE);
}
