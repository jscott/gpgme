From: Daniel Kahn Gillmor <dkg@fifthhorseman.net>
Date: Thu, 23 Jan 2020 17:04:43 -0500
Subject: gpg: Send --with-keygrip when listing keys

* src/engine-gpg.c (gpg_keylist_build_options): Always pass
--with-keygrip when listing keys.

--

Some older versions of GnuPG (at least gpg in version
2.2.12-1+deb10u1) appear to unilaterally emit the keygrip, and it's
certainly useful to know that programmatically it will always be
present in the output needed.

However, as of GnuPG 2.2.19-1, the following command does not emit the
keygrip for non-secret keys:

   gpg --with-colons --with-secret --list-keys

This change fixes tests/json/t-json by making the output comply with
tests/json/t-keylist-secret.out.json correctly.

Signed-off-by: Daniel Kahn Gillmor <dkg@fifthhorseman.net>
---
 src/engine-gpg.c | 1 +
 1 file changed, 1 insertion(+)

diff --git a/src/engine-gpg.c b/src/engine-gpg.c
index af2533d..f702eb4 100644
--- a/src/engine-gpg.c
+++ b/src/engine-gpg.c
@@ -2989,6 +2989,7 @@ gpg_keylist_build_options (engine_gpg_t gpg, int secret_only,
   gpg_error_t err;
 
   err = add_arg (gpg, "--with-colons");
+  err = add_arg (gpg, "--with-keygrip");
 
   /* Since gpg 2.1.15 fingerprints are always printed, thus there is
    * no more need to explicitly request them.  */
